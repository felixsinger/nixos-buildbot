{ config, lib, pkgs, modulesPath, ... }:

{
	imports = [
		(modulesPath + "/profiles/qemu-guest.nix")
	];

	fileSystems = {
		"/" = {
			device = "/dev/disk/by-uuid/8b4a4a11-029f-452c-9cc3-64594ef4db05";
			fsType = "btrfs";
			options = [ "noatime" "compress=zstd" "subvol=/" ];
		};
		"/boot" = {
			device = "/dev/disk/by-uuid/8b4a4a11-029f-452c-9cc3-64594ef4db05";
			fsType = "btrfs";
			options = [ "noatime" "compress=zstd" "subvol=/boot" ];
		};
		"/home" = {
			device = "/dev/disk/by-uuid/8b4a4a11-029f-452c-9cc3-64594ef4db05";
			fsType = "btrfs";
			options = [ "noatime" "compress=zstd" "subvol=/home" ];
		};
		"/tmp" = {
			device = "/dev/disk/by-uuid/8b4a4a11-029f-452c-9cc3-64594ef4db05";
			fsType = "btrfs";
			options = [ "noatime" "compress=zstd" "subvol=/tmp" ];
		};
		"/var" = {
			device = "/dev/disk/by-uuid/8b4a4a11-029f-452c-9cc3-64594ef4db05";
			fsType = "btrfs";
			options = [ "noatime" "compress=zstd" "subvol=/var" ];
		};
		"/var/log" = {
			device = "/dev/disk/by-uuid/8b4a4a11-029f-452c-9cc3-64594ef4db05";
			fsType = "btrfs";
			options = [ "noatime" "compress=zstd" "subvol=/var/log" ];
		};
		"/var/tmp" = {
			device = "/dev/disk/by-uuid/8b4a4a11-029f-452c-9cc3-64594ef4db05";
			fsType = "btrfs";
			options = [ "noatime" "compress=zstd" "subvol=/var/tmp" ];
		};
	};
}
