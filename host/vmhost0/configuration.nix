{ pkgs, ... }:

{
	imports = [
		./partitions.nix
		../../profile/vmhost.nix
		../../template/buildbot/master.nix
	];

	system.stateVersion = "21.05";

	console = {
		font = "Lat2-Terminus16";
		keyMap = "de";
	};

	time.timeZone = "Europe/Berlin";

	services.buildbot-master = {
		titleUrl = "http://ci.uefi.rip:8010";
		buildbotUrl = "http://ci.uefi.rip:8010/";
		listenAddress = "136.243.83.70";
	};

	networking = {
		hostName = "bb-vmhost0";
		useDHCP = false;
		bridges."br-wan".interfaces = [ "eno1" ];
		interfaces = {
			"br-wan" = {
				ipv4.addresses = [{
					address = "136.243.83.70";
					prefixLength = 26;
				}];
				ipv6.addresses = [{
					address = "2a01:4f8:212:3162::1";
					prefixLength = 64;
				}];
			};
		};
		defaultGateway = {
			address = "136.243.83.65";
			interface = "br-wan";
		};
		defaultGateway6 = {
			address = "fe80::1";
			interface = "br-wan";
		};
#		firewall.allowedTCPPorts = [ 8010 ];
	};

	boot = {
		loader.grub.devices = [
			"/dev/disk/by-id/wwn-0x5000cca22df28d03"
			"/dev/disk/by-id/wwn-0x5000cca22df27cfa"
		];
		initrd.availableKernelModules = [ "ahci" "sd_mod" ];
		kernelModules = [ "kvm-intel" ];
	};

	environment.systemPackages = with pkgs; [
		htop
		usbutils
		pciutils
		tmux
		neovim
		hdparm
		smartmontools
		parted
		cryptsetup
		btrfs-progs
		ntfs3g
		exfat
		e2fsprogs
		curl
		gptfdisk
		wget
		gitAndTools.gitFull
		gitAndTools.tig
		python3Full
		p7zip
		unzip
		whois
	];
}
