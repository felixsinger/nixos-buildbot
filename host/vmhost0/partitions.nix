{ config, lib, pkgs, ... }:

{
	fileSystems = {
		"/" = {
			device = "/dev/disk/by-uuid/6d1e4d33-55f5-40b5-bfaf-a1223d82bd5d";
			fsType = "btrfs";
			options = [ "compress=zstd" "noatime" "subvol=/" ];
		};
		"/boot" = {
			device = "/dev/disk/by-uuid/6d1e4d33-55f5-40b5-bfaf-a1223d82bd5d";
			fsType = "btrfs";
			options = [ "compress=zstd" "noatime" "subvol=/boot" ];
		};
		"/home" = {
			device = "/dev/disk/by-uuid/6d1e4d33-55f5-40b5-bfaf-a1223d82bd5d";
			fsType = "btrfs";
			options = [ "compress=zstd" "noatime" "subvol=/home" ];
		};
		"/tmp" = {
			device = "/dev/disk/by-uuid/6d1e4d33-55f5-40b5-bfaf-a1223d82bd5d";
			fsType = "btrfs";
			options = [ "compress=zstd" "noatime" "subvol=/tmp" ];
		};
		"/var" = {
			device = "/dev/disk/by-uuid/6d1e4d33-55f5-40b5-bfaf-a1223d82bd5d";
			fsType = "btrfs";
			options = [ "compress=zstd" "noatime" "subvol=/var" ];
		};
		"/var/log" = {
			device = "/dev/disk/by-uuid/6d1e4d33-55f5-40b5-bfaf-a1223d82bd5d";
			fsType = "btrfs";
			options = [ "compress=zstd" "noatime" "subvol=/var/log" ];
		};
		"/var/tmp" = {
			device = "/dev/disk/by-uuid/6d1e4d33-55f5-40b5-bfaf-a1223d82bd5d";
			fsType = "btrfs";
			options = [ "compress=zstd" "noatime" "subvol=/var/tmp" ];
		};
		"/storage/local" = {
			device = "/dev/disk/by-uuid/6d1e4d33-55f5-40b5-bfaf-a1223d82bd5d";
			fsType = "btrfs";
			options = [ "compress=zstd" "noatime" "subvol=/storage/local" ];
		};
	};
}
