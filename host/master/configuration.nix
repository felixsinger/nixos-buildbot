{ config, pkgs, ... }:

{
	imports = [
		./partitions.nix
		../../profile/master.nix
	];

	system.stateVersion = "21.05";

	services.buildbot-master = {
		titleUrl = "http://10.0.0.50:8010";
		buildbotUrl = "http://10.0.0.50:8010/";
		listenAddress = "10.0.0.50";
	};

	console = {
		font = "Lat2-Terminus16";
		keyMap = "de";
	};

	time.timeZone = "Europe/Berlin";

	networking = {
		hostName = "bb-master";
		useDHCP = false;
		interfaces."enp1s0".ipv4.addresses = [{
			address = "10.0.0.50";
			prefixLength = 24;
		}];
		defaultGateway = {
			address = "10.0.0.1";
			interface = "enp1s0";
		};
	};

	boot = {
		loader.grub.device = "/dev/vda";
		initrd.availableKernelModules = [ "ahci" "virtio_pci" "sd_mod" ];
	};

	users.users.cbadmin.openssh.authorizedKeys.keys = [
		"ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOj+ND1fjcaEw/lghRrHnAMUTp0sBQi7d5Y6flFnG4gq cbadmin@bb-vmhost0"
	];

	environment.systemPackages = with pkgs; [
		htop
		usbutils
		pciutils
		tmux
		neovim
		hdparm
		smartmontools
		parted
		cryptsetup
		btrfs-progs
		ntfs3g
		exfat
		e2fsprogs
		curl
		gptfdisk
		wget
		gitAndTools.gitFull
		gitAndTools.tig
		python3Full
		p7zip
		unzip
		whois
	];
}
