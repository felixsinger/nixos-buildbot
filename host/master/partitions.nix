{ config, lib, pkgs, modulesPath, ... }:

{
	imports = [
		(modulesPath + "/profiles/qemu-guest.nix")
	];

	fileSystems = {
		"/" = {
			device = "/dev/disk/by-uuid/db66474b-6a0f-483f-b775-c1606f67881e";
			fsType = "btrfs";
			options = [ "noatime" "compress=zstd" "subvol=/" ];
		};
		"/boot" = {
			device = "/dev/disk/by-uuid/db66474b-6a0f-483f-b775-c1606f67881e";
			fsType = "btrfs";
			options = [ "noatime" "compress=zstd" "subvol=/boot" ];
		};
		"/home" = {
			device = "/dev/disk/by-uuid/db66474b-6a0f-483f-b775-c1606f67881e";
			fsType = "btrfs";
			options = [ "noatime" "compress=zstd" "subvol=/home" ];
		};
		"/tmp" = {
			device = "/dev/disk/by-uuid/db66474b-6a0f-483f-b775-c1606f67881e";
			fsType = "btrfs";
			options = [ "noatime" "compress=zstd" "subvol=/tmp" ];
		};
		"/var" = {
			device = "/dev/disk/by-uuid/db66474b-6a0f-483f-b775-c1606f67881e";
			fsType = "btrfs";
			options = [ "noatime" "compress=zstd" "subvol=/var" ];
		};
		"/var/log" = {
			device = "/dev/disk/by-uuid/db66474b-6a0f-483f-b775-c1606f67881e";
			fsType = "btrfs";
			options = [ "noatime" "compress=zstd" "subvol=/var/log" ];
		};
		"/var/tmp" = {
			device = "/dev/disk/by-uuid/db66474b-6a0f-483f-b775-c1606f67881e";
			fsType = "btrfs";
			options = [ "noatime" "compress=zstd" "subvol=/var/tmp" ];
		};
	};
}
