{
	services = {
		timesyncd.enable = false;
		chrony = {
			enable = true;
		};
	};
}
