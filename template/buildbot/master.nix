{ pkgs, ... }:

{
	services.buildbot-master = {
		enable = true;
		title = "coreboot CI";
		pythonPackages = pythonPackages: with pkgs.python3Packages; [ libvirt ];
		extraConfig = (builtins.readFile ./master.cfg);
	};
}
