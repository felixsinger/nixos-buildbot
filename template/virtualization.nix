{ pkgs, ... }:

{
	virtualisation = {
		lxc.enable = true;
		libvirtd = {
			enable = true;
			onBoot = "ignore";
			onShutdown = "shutdown";
			qemuRunAsRoot = false;
			qemuPackage = pkgs.qemu;
		};
	};
}
