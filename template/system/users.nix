{ pkgs, ... }:

{
	users = {
		groups = {
			cbadmin = { };
			configadmin = { };
		};
		users = {
			configadmin = {
				group = "configadmin";
				extraGroups = [ "users" ];
				createHome = true;
				home = "/home/configadmin";
				useDefaultShell = true;
				isNormalUser = true;
			};
			cbadmin = {
				group = "cbadmin";
				extraGroups = [ "users" "wheel" ];
				createHome = true;
				home = "/home/cbadmin";
				useDefaultShell = true;
				isNormalUser = true;
				openssh.authorizedKeys.keys = [
					"ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINTfzL1Wbxn+FfW94rHPUPen20/U6hV7zwHTirSiRRcu felixsinger@elara"
					"ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEaT017EMc0zjh67Yb0spev+iLeIcaI80wBUzQz6ORtD me@thebe"
				];
			};
		};
	};
}
