{
	imports = [
		../template/chrony.nix
		../template/grub.nix
		../template/openssh.nix
#		../template/security/exec_wheel_only.nix
#		../template/security/lock_kernel_modules.nix
#		../template/security/protect_kernel_image.nix
		../template/system/autoupgrade.nix
		../template/system/gc.nix
		../template/system/locale.nix
		../template/system/users.nix
	];

	networking.nameservers = [
		"8.8.8.8"
		"4.4.4.4"
		"2001:4860:4860::8888"
		"2001:4860:4860::8844"
	];
}
