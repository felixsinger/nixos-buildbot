{
	imports = [
		./common.nix
		../template/glusterfs.nix
		../template/virtualization.nix
	];

	users.users.cbadmin.extraGroups = [ "libvirtd" ];
}
